#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <stdexcept>

#include "Animation.h"
#include "Keys.h"

#define PI 3.14159265f

const GLuint WIDTH = 800, HEIGHT = 600;
Keys keys;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if(action != GLFW_REPEAT)
		keys.update(key, action);
}

int main()
{
	if (glfwInit() != GL_TRUE)
	{
		std::cerr << "GLFW" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	try
	{
		GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Cogwheel Test", nullptr, nullptr);
		if (!window)
			throw std::runtime_error("Failed to create GLFW window");

		glfwMakeContextCurrent(window);
		glfwSetKeyCallback(window, key_callback);

		glewExperimental = GL_TRUE;
		if (glewInit() != GLEW_OK)
			throw std::runtime_error("Failed to init GLEW");

		glViewport(0, 0, WIDTH, HEIGHT);

		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		Animation anim(50.f, 70.f, 16, 30.f, 300.f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 2.);
		anim.start();

		while (!glfwWindowShouldClose(window))
		{
			glfwPollEvents();

			glClearColor(0.f, 0.f, 0.f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			
			anim.process_input(keys);
			anim.update();
			anim.render();

			glfwSwapBuffers(window);
		}
	}
	catch(std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
	}

	glfwTerminate();
	return 0;
}