#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

class Camera
{
public:
	Camera(glm::vec3 pos, glm::vec3 look_at,
		   GLfloat fov, GLfloat ratio,
		   GLfloat near, GLfloat far);

	glm::mat4 getMatrix() const;
	glm::mat4 getProjection() const { return proj_; }
	glm::mat4 getView() const { return view_; }
	glm::vec3 getPos() const { return pos_; }
	glm::vec3 getDir() const;

	void strafe(GLfloat ds);
	void pedestal(GLfloat ds);
	void toward(GLfloat ds);
	void forward(GLfloat ds);
	void orbit(GLfloat dang);

	void update();

private:
	glm::mat4 proj_;
	glm::mat4 view_;

	glm::vec3 pos_;
	glm::vec3 look_;

	glm::vec3 dir_;
	glm::vec3 up_;
	glm::vec3 right_;

	glm::vec3 forw_;
};

