#include "Cuboid.h"

Cuboid::Cuboid(GLfloat w, GLfloat h, GLfloat d, Color c, FaceDir fdir)
	: width_(w),
	  height_(h),
	  depth_(d),
	  color_(c)
{
	vertices_ = std::vector<Vertex>(24);
	glm::vec3 normal = (fdir == OUTWARDS) ? glm::vec3(0.f, 0.f, -1.f) : glm::vec3(0.f, 0.f, 1.f);

	const glm::vec2 wall_LL(0.f, 1.f);
	const glm::vec2 wall_UL(0.f, 0.f);
	const glm::vec2 wall_UR(0.5f, 0.f);
	const glm::vec2 wall_LR(0.5f, 1.f);

	const glm::vec2 ceil_LL(0.5f, 1.f);
	const glm::vec2 ceil_UL(0.5f, 0.f);
	const glm::vec2 ceil_UR(1.f, 0.f);
	const glm::vec2 ceil_LR(1.f, 1.f);

	//lower face
	vertices_[0] = Vertex(0.f, 0.f, 0.f, color_, normal, ceil_LL);
	vertices_[1] = Vertex(0.f, height_, 0.f, color_, normal, ceil_UL);
	vertices_[2] = Vertex(width_, height_, 0.f, color_, normal, ceil_UR);
	vertices_[3] = Vertex(width_, 0.f, 0.f, color_, normal, ceil_LR);

	//upper face
	normal = (fdir == OUTWARDS) ? glm::vec3(0.f, 0.f, 1.f) : glm::vec3(0.f, 0.f, -1.f);
	vertices_[4] = Vertex(0.f, 0.f, depth_, color_, normal, ceil_UL);
	vertices_[5] = Vertex(width_, 0.f, depth_, color_, normal, ceil_UR);
	vertices_[6] = Vertex(width_, height_, depth_, color_, normal, ceil_LR);
	vertices_[7] = Vertex(0.f, height_, depth_, color_, normal, ceil_LL);

	//front face
	normal = (fdir == OUTWARDS) ? glm::vec3(0.f, -1.f, 0.f) : glm::vec3(0.f, 1.f, 0.f);
	vertices_[8] = Vertex(0.f, 0.f, 0.f, color_, normal, wall_LR);
	vertices_[9] = Vertex(width_, 0.f, 0.f, color_, normal, wall_LL);
	vertices_[10] = Vertex(width_, 0.f, depth_, color_, normal, wall_UL);
	vertices_[11] = Vertex(0.f, 0.f, depth_, color_, normal, wall_UR);

	//back face
	normal = (fdir == OUTWARDS) ? glm::vec3(0.f, 1.f, 0.f) : glm::vec3(0.f, -1.f, 0.f);
	vertices_[12] = Vertex(0.f, height_, 0.f, color_, normal, wall_LL);
	vertices_[13] = Vertex(0.f, height_, depth_, color_, normal, wall_UL);
	vertices_[14] = Vertex(width_, height_, depth_, color_, normal, wall_UR);
	vertices_[15] = Vertex(width_, height_, 0.f, color_, normal, wall_LR);

	//left face
	normal = (fdir == OUTWARDS) ? glm::vec3(-1.f, 0.f, 0.f) : glm::vec3(1.f, 0.f, 0.f);
	vertices_[16] = Vertex(0.f, 0.f, 0.f, color_, normal, wall_LL);
	vertices_[17] = Vertex(0.f, 0.f, depth_, color_, normal, wall_UL);
	vertices_[18] = Vertex(0.f, height_, depth_, color_, normal, wall_UR);
	vertices_[19] = Vertex(0.f, height_, 0.f, color_, normal, wall_LR);

	//right face
	normal = (fdir == OUTWARDS) ? glm::vec3(1.f, 0.f, 0.f) : glm::vec3(-1.f, 0.f, 0.f);
	vertices_[20] = Vertex(width_, 0.f, 0.f, color_, normal, wall_LR);
	vertices_[21] = Vertex(width_, height_, 0.f, color_, normal, wall_LL);
	vertices_[22] = Vertex(width_, height_, depth_, color_, normal, wall_UL);
	vertices_[23] = Vertex(width_, 0.f, depth_, color_, normal, wall_UR);

	switch (fdir)
	{
	case FaceDir::OUTWARDS:
		indices_ = { 0, 1, 2, 0, 2, 3,
					 4, 5, 6, 4, 6, 7,
					 8, 9, 10, 8, 10, 11,
					 12, 13, 14, 12, 14, 15,
					 16, 17, 18, 16, 18, 19,
					 20, 21, 22, 20, 22, 23 };
		break;

	case FaceDir::INWARDS:
		indices_ = { 0, 2, 1, 0, 3, 2,
					 4, 6, 5, 4, 7, 6,
					 8, 10, 9, 8, 11, 10,
					 12, 14, 13, 12, 15, 14,
					 16, 18, 17, 16, 19, 18,
					 20, 22, 21, 20, 23, 22 };
		break;
	}
}
