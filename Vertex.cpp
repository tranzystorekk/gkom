#include "Vertex.h"

#include <cmath>

#define PI 3.14159265f

Vertex::Vertex() : x_(0.f), y_(0.f), z_(0.f)
{
}

Vertex::Vertex(GLfloat x, GLfloat y, GLfloat z, Color c, glm::vec3 n, glm::vec2 tex)
	: x_(x),
	  y_(y),
	  z_(z),
	  color_(c),
	  norm_(n),
	  textcord_(tex)
{
}

Vertex::Vertex(GLfloat x, GLfloat y, GLfloat z, glm::vec3 n)
	: x_(x),
	  y_(y),
	  z_(z),
	  norm_(n)
{
}

inline GLfloat rad(GLfloat deg)
{
	return (deg * PI) / 180.f;
}

Vertex Vertex::clone() const
{
	return *this;
}

Vertex& Vertex::rotateXY(GLfloat deg)
{
	GLfloat angle = rad(deg);
	GLfloat x = x_;
	GLfloat y = y_;

	x_ = x * cos(angle) - y * sin(angle);
	y_ = x * sin(angle) + y * cos(angle);

	return *this;
}

Vertex& Vertex::scaleXY(float ratio)
{
	x_ *= ratio;
	y_ *= ratio;

	return *this;
}

Vertex& Vertex::move(GLfloat dx, GLfloat dy, GLfloat dz)
{
	x_ += dx;
	y_ += dy;
	z_ += dz;

	return *this;
}

glm::vec3 Vertex::getVec3() const
{
	return glm::vec3(x_, y_, z_);
}

Color Color::fromRGB8(int r, int g, int b)
{
	return Color(r / 255.f, g / 255.f, b / 255.f);
}
