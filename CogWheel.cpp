#include "CogWheel.h"

#include <glm/gtc/matrix_transform.hpp>

CogWheel::CogWheel(GLfloat r, GLfloat d, GLfloat w, GLuint n, Color c, Shape shape)
	: radius_(r),
	  d_(d),
	  ncogs_(n),
	  width_(w),
	  shape_(shape),
	  color_(c),
	  rotation_(0.f)
{
	switch (shape_)
	{
	case STAR:
		gen_star();
		break;

	case SPIKES:
		gen_spikes();
		break;

	case COGS:
		gen_cogs();
		break;
	}
}

void CogWheel::rotate(GLfloat deg)
{
	rotation_ += deg;

	if (rotation_ > 360.f)
		rotation_ -= 360.f;
	else if (rotation_ < 0.f)
		rotation_ += 360.f;

	Object::rotate(deg, glm::vec3(0.f, 0.f, 1.f));
}

void CogWheel::gen_star()
{
	vertices_ = std::vector<Vertex>(2 + 2*(2 * ncogs_) + 4 * 2 * ncogs_); //2 face centers, 2 main faces with 2 vxs per arm, 2n side faces with 4 vxs per face
	indices_ = std::vector<GLuint>(24 * ncogs_);

	const glm::vec3 UPPER_NORM(0.f, 0.f, 1.f);
	const glm::vec3 LOWER_NORM(0.f, 0.f, -1.f);

	//vertices_[0] = (0, 0, 0)
	vertices_[1] = Vertex(0.f, 0.f, width_);
	vertices_[0].setNorm(LOWER_NORM).setColor(color_);
	vertices_[1].setNorm(UPPER_NORM).setColor(color_);

	vertices_[2] = Vertex(0.f, radius_, 0.f);
	vertices_[2 + 2 * ncogs_] = Vertex(0.f, radius_, width_);
	vertices_[2].setNorm(LOWER_NORM).setColor(color_);
	vertices_[2 + 2 * ncogs_].setNorm(UPPER_NORM).setColor(color_);

	//VERTICES
	//a) main faces
	const Vertex& ORIGIN_LOWER = vertices_[2];
	const Vertex& ORIGIN_UPPER = vertices_[2 + 2 * ncogs_];

	int n = 2 * ncogs_ + 2;
	int offset = 2 * ncogs_;

	GLfloat angle = -180.f / ncogs_;
	const GLfloat increment = angle;

	const GLfloat ratio = 1.f - d_ / radius_;

	int i = 3;

	for (; i < n; ++i)
	{
		Vertex& v = vertices_[i];
		Vertex& w = vertices_[i + offset];

		v = ORIGIN_LOWER.clone().rotateXY(angle);
		w = ORIGIN_UPPER.clone().rotateXY(angle);

		if (i % 2)
		{
			v.scaleXY(ratio);
			w.scaleXY(ratio);
		}

		angle += increment;
	}

	//b) side faces
	n = vertices_.size() - 4;
	i = 4 * ncogs_ + 2;

	for (int j = 2; i < n; i += 4, ++j)
	{
		glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[j+offset].getVec3() - vertices_[j].getVec3(),
													   vertices_[j+1].getVec3() - vertices_[j].getVec3()));

		vertices_[i] = vertices_[j].clone().setNorm(facenorm);
		vertices_[i + 1] = vertices_[j + offset].clone().setNorm(facenorm);
		vertices_[i + 2] = vertices_[j + 1].clone().setNorm(facenorm);
		vertices_[i + 3] = vertices_[j + offset + 1].clone().setNorm(facenorm);
	}

	glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[4 * ncogs_ + 1].getVec3() - vertices_[2*ncogs_ + 1].getVec3(),
												   vertices_[2].getVec3() - vertices_[2 * ncogs_ + 1].getVec3()));

	n = vertices_.size();

	vertices_[n - 4] = vertices_[2 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 3] = vertices_[4 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 2] = vertices_[2].clone().setNorm(facenorm);
	vertices_[n - 1] = vertices_[2 + 2 * ncogs_].clone().setNorm(facenorm);

	//INDICES
	//lower face; CW winding
	i = 3;
	n = 2 * ncogs_;
	int vnumber = 2;
	
	indices_[0] = 0;
	indices_[1] = 2 * ncogs_ + 1;
	indices_[2] = 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 0;
		indices_[i + 1] = vnumber;
		indices_[i + 2] = vnumber + 1;

		++vnumber;
	}

	//upper face; CCW winding
	indices_[i] = 1;
	indices_[i + 1] = 2 * ncogs_ + 2;
	indices_[i + 2] = 4 * ncogs_ + 1;

	i += 3;
	vnumber = 2 * ncogs_ + 2;
	
	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 1;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber;

		++vnumber;
	}

	//side faces; alternating winding
	n = 2 * ncogs_;
	vnumber = 4 * ncogs_ + 2;

	for (int j = 0; j < n; ++j, i += 6)
	{
		indices_[i] = vnumber;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber + 2;

		indices_[i + 3] = vnumber + 1;
		indices_[i + 4] = vnumber + 3;
		indices_[i + 5] = vnumber + 2;

		vnumber += 4;
	}
}

void CogWheel::gen_spikes()
{
	vertices_ = std::vector<Vertex>(2 + 2 * (3 * ncogs_) + 4 * 3 * ncogs_);
	indices_ = std::vector<GLuint>(36 * ncogs_);

	const glm::vec3 UPPER_NORM(0.f, 0.f, 1.f);
	const glm::vec3 LOWER_NORM(0.f, 0.f, -1.f);

	//vertices_[0] = (0, 0, 0)
	vertices_[1] = Vertex(0.f, 0.f, width_);
	vertices_[0].setNorm(LOWER_NORM).setColor(color_);
	vertices_[1].setNorm(UPPER_NORM).setColor(color_);

	vertices_[2] = Vertex(0.f, radius_, 0.f);
	vertices_[2 + 3 * ncogs_] = Vertex(0.f, radius_, width_);
	vertices_[2].setNorm(LOWER_NORM).setColor(color_);
	vertices_[2 + 3 * ncogs_].setNorm(UPPER_NORM).setColor(color_);

	//VERTICES
	//a) main faces
	const Vertex& ORIGIN_LOWER = vertices_[2];
	const Vertex& ORIGIN_UPPER = vertices_[2 + 3 * ncogs_];

	int n = 3 * ncogs_ + 2;
	int offset = 3 * ncogs_;

	GLfloat angle = -120.f / ncogs_;
	const GLfloat increment = angle;

	const GLfloat ratio = 1.f - d_ / radius_;

	int i = 3;

	for (; i < n; ++i)
	{
		Vertex& v = vertices_[i];
		Vertex& w = vertices_[i + offset];

		v = ORIGIN_LOWER.clone().rotateXY(angle);
		w = ORIGIN_UPPER.clone().rotateXY(angle);

		if (i % 3 < 2)
		{
			v.scaleXY(ratio);
			w.scaleXY(ratio);
		}

		angle += increment;
	}

	//b) side faces
	n = vertices_.size() - 4;
	i = 6 * ncogs_ + 2;

	for (int j = 2; i < n; i += 4, ++j)
	{
		glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[j + offset].getVec3() - vertices_[j].getVec3(),
													   vertices_[j + 1].getVec3() - vertices_[j].getVec3()));

		vertices_[i] = vertices_[j].clone().setNorm(facenorm);
		vertices_[i + 1] = vertices_[j + offset].clone().setNorm(facenorm);
		vertices_[i + 2] = vertices_[j + 1].clone().setNorm(facenorm);
		vertices_[i + 3] = vertices_[j + offset + 1].clone().setNorm(facenorm);
	}

	glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[6 * ncogs_ + 1].getVec3() - vertices_[3 * ncogs_ + 1].getVec3(),
												   vertices_[2].getVec3() - vertices_[3 * ncogs_ + 1].getVec3()));

	n = vertices_.size();

	vertices_[n - 4] = vertices_[3 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 3] = vertices_[6 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 2] = vertices_[2].clone().setNorm(facenorm);
	vertices_[n - 1] = vertices_[2 + 3 * ncogs_].clone().setNorm(facenorm);

	//INDICES
	//lower face; CW winding
	i = 3;
	n = 3 * ncogs_;
	int vnumber = 2;

	indices_[0] = 0;
	indices_[1] = 3 * ncogs_ + 1;
	indices_[2] = 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 0;
		indices_[i + 1] = vnumber;
		indices_[i + 2] = vnumber + 1;

		++vnumber;
	}

	//upper face; CCW winding
	indices_[i] = 1;
	indices_[i + 1] = 3 * ncogs_ + 2;
	indices_[i + 2] = 6 * ncogs_ + 1;

	i += 3;
	vnumber = 3 * ncogs_ + 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 1;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber;

		++vnumber;
	}

	//side faces; alternating winding
	n = 3 * ncogs_;
	vnumber = 6 * ncogs_ + 2;

	for (int j = 0; j < n; ++j, i += 6)
	{
		indices_[i] = vnumber;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber + 2;

		indices_[i + 3] = vnumber + 1;
		indices_[i + 4] = vnumber + 3;
		indices_[i + 5] = vnumber + 2;

		vnumber += 4;
	}
}

void CogWheel::gen_cogs()
{
	vertices_ = std::vector<Vertex>(2 + 2 * (4 * ncogs_) + 4 * 4 * ncogs_);
	indices_ = std::vector<GLuint>(48 * ncogs_);

	const glm::vec3 UPPER_NORM(0.f, 0.f, 1.f);
	const glm::vec3 LOWER_NORM(0.f, 0.f, -1.f);

	//vertices_[0] = (0, 0, 0)
	vertices_[1] = Vertex(0.f, 0.f, width_);
	vertices_[0].setNorm(LOWER_NORM).setColor(color_);
	vertices_[1].setNorm(UPPER_NORM).setColor(color_);

	vertices_[2] = Vertex(0.f, radius_, 0.f);
	vertices_[2 + 4 * ncogs_] = Vertex(0.f, radius_, width_);
	vertices_[2].setNorm(LOWER_NORM).setColor(color_);
	vertices_[2 + 4 * ncogs_].setNorm(UPPER_NORM).setColor(color_);

	//VERTICES
	//a) main faces
	const Vertex& ORIGIN_LOWER = vertices_[2];
	const Vertex& ORIGIN_UPPER = vertices_[2 + 4 * ncogs_];

	int n = 4 * ncogs_ + 2;
	int offset = 4 * ncogs_;

	GLfloat angle = -90.f / ncogs_;
	const GLfloat increment = angle;

	const GLfloat ratio = 1.f - d_ / radius_;

	int i = 3;

	for (; i < n; ++i)
	{
		Vertex& v = vertices_[i];
		Vertex& w = vertices_[i + offset];

		v = ORIGIN_LOWER.clone().rotateXY(angle);
		w = ORIGIN_UPPER.clone().rotateXY(angle);

		if (i % 4 < 2)
		{
			v.scaleXY(ratio);
			w.scaleXY(ratio);
		}

		angle += increment;
	}

	//b) side faces
	n = vertices_.size() - 4;
	i = 8 * ncogs_ + 2;

	for (int j = 2; i < n; i += 4, ++j)
	{
		glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[j + offset].getVec3() - vertices_[j].getVec3(),
													   vertices_[j + 1].getVec3() - vertices_[j].getVec3()));

		vertices_[i] = vertices_[j].clone().setNorm(facenorm);
		vertices_[i + 1] = vertices_[j + offset].clone().setNorm(facenorm);
		vertices_[i + 2] = vertices_[j + 1].clone().setNorm(facenorm);
		vertices_[i + 3] = vertices_[j + offset + 1].clone().setNorm(facenorm);
	}

	glm::vec3 facenorm = glm::normalize(glm::cross(vertices_[8 * ncogs_ + 1].getVec3() - vertices_[4 * ncogs_ + 1].getVec3(),
												   vertices_[2].getVec3() - vertices_[4 * ncogs_ + 1].getVec3()));

	n = vertices_.size();

	vertices_[n - 4] = vertices_[4 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 3] = vertices_[8 * ncogs_ + 1].clone().setNorm(facenorm);
	vertices_[n - 2] = vertices_[2].clone().setNorm(facenorm);
	vertices_[n - 1] = vertices_[2 + 4 * ncogs_].clone().setNorm(facenorm);

	//INDICES
	//lower face; CW winding
	i = 3;
	n = 4 * ncogs_;
	int vnumber = 2;

	indices_[0] = 0;
	indices_[1] = 4 * ncogs_ + 1;
	indices_[2] = 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 0;
		indices_[i + 1] = vnumber;
		indices_[i + 2] = vnumber + 1;

		++vnumber;
	}

	//upper face; CCW winding
	indices_[i] = 1;
	indices_[i + 1] = 4 * ncogs_ + 2;
	indices_[i + 2] = 8 * ncogs_ + 1;

	i += 3;
	vnumber = 4 * ncogs_ + 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 1;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber;

		++vnumber;
	}

	//side faces; alternating winding
	n = 4 * ncogs_;
	vnumber = 8 * ncogs_ + 2;

	for (int j = 0; j < n; ++j, i += 6)
	{
		indices_[i] = vnumber;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber + 2;

		indices_[i + 3] = vnumber + 1;
		indices_[i + 4] = vnumber + 3;
		indices_[i + 5] = vnumber + 2;

		vnumber += 4;
	}
}
