#pragma once
#include "Object.h"

class Cylinder : public Object
{
public:
	Cylinder(GLfloat r, GLfloat w, GLuint n, Color c);

private:
	GLfloat radius_;
	GLfloat width_;
	GLuint ncorners_;
	Color color_;
};

